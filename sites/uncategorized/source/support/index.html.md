---
layout: markdown_page
title: Support
description: "Visit the GitLab support page to find product support links and to contact the support team."
---

{:.no_toc}

GitLab offers a variety of support options for all customers and users on both paid
and free tiers. You should be able to find help using the resources linked below, regardless
of how you use GitLab. There are many ways to [contact Support](#contact-support), but
the first step for most people should be to [search our documentation](https://docs.gitlab.com).

If you can't find an answer to your question, or you are affected by an outage, then
customers who are in a **paid** tier should start by consulting our [statement of support](/support/statement-of-support.html) while being mindful of what is outside of the scope of support. Please understand that any support that might be offered beyond the scope defined there is done
at the discretion of the agent or engineer and is provided as a courtesy.

If you're using one of GitLab's **free** options, please refer to the
appropriate section for free users of either [self-managed GitLab](/support/statement-of-support.html#core-and-community-edition-users) or [on GitLab.com](/support/statement-of-support.html#free-plan-users).

Note that free GitLab Ultimate Self-managed and SaaS  granted through trials or as part of our [GitLab for Education](/solutions/education/), [GitLab for Open Source](/solutions/open-source/), or [GitLab for Startups](/solutions/startups/) programs do not come with technical support. Technical support for open source, education, and startup accounts can, however, be purchased at a significant discount by [contacting Sales](/sales/). If you are facing issues relating to billing, purchasing, subscriptions, or licensing, please refer to [Issues with billing, purchasing, subscriptions or licenses](#issues-with-billing-purchasing-subscriptions-or-licenses).

## Contact Support
{:.no_toc}

### Issues with billing, purchasing, subscriptions or licenses
{:.no_toc}

| Plan      | Support level (First Response Time)              | How to get in touch |
|-----------|----------------------------|---------------------|
| All plans and purchases | First reply within 8 hours, 24x5 | Open a Support Ticket on the [GitLab Support Portal](https://support.gitlab.com) and select "[Licensing and Renewals Problems](https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360000071293)" |

### Self-managed (hosted on your own site/server)
{:.no_toc}

| Plan | Support Level | How to get in touch |
|------|----------------|---|
| Core | Community Support | Open a thread in the  [GitLab Community Forum](https://forum.gitlab.com) |
| Premium and Ultimate | [Priority Support](#priority-support) <br /> Tiered reply times based on [definitions of support impact](#definitions-of-support-impact) | Open a Support Ticket on the [GitLab Support Portal](https://support.gitlab.com) <br/> For emergency requests, see the note in the [How to Trigger Emergency Support](#how-to-trigger-emergency-support) |
| US Federal | [US Federal Support](#us-federal-support) | Open a Support Ticket on the [GitLab Support Portal](https://gitlab-federal-support.zendesk.com/) <br/> For emergency requests, see the note in the [US Federal Support description](#us-federal-emergency-support) |

### GitLab SaaS (GitLab.com)
{:.no_toc}

| Plan | Support Level | How to get in touch |
|------|----------------|---|
| Free |  Community Support | Open a thread in the [GitLab Community Forum](https://forum.gitlab.com) |
| Premium and Ultimate | [Priority Support](#priority-support) <br /> Tiered reply times based on [definitions of support impact](#definitions-of-support-impact) | Open a Support Ticket on the [GitLab Support Portal](https://support.gitlab.com) |


### Legacy Plans
{:.no_toc}

These plans are no longer offered for new purchases, but still maintain their historical support levels.

| Plan | Support Level | How to get in touch |
|------|----------------|---|
| Bronze (GitLab.com) | [Standard Support](#standard-support-legacy) <br/> Reply within 24 hours 24x5 | Open a Support Ticket on the [GitLab Support Portal](https://support.gitlab.com) |
| Starter (GitLab Self-managed) |  [Standard Support](#standard-support-legacy) <br/> Reply within 24 hours 24x5 | Open a Support Ticket on the [GitLab Support Portal](https://support.gitlab.com) |


### Partner Support
{:.no_toc}
If you are an Open or Select Partner with GitLab and creating a ticket on behalf of a customer, please include the customer's organisation and email address when creating the ticket.

### Proving your Support Entitlement
{:.no_toc}
Depending on how you purchased, upon the creation of your first ticket GitLab Support may **not** be able to automatically detect your support entitlement. If that's the case, you will be asked to provide evidence that you have an active license or subscription.

#### For GitLab.com Users
{:.no_toc}

To ensure that we can match you with your GitLab.com subscription when opening a support ticket, please:
- include your GitLab.com username; AND
- use the primary email address associated with your account; AND
- reference a path within a GitLab.com group that has a valid subscription associated with it (e.g., a link to a problematic pipeline or MR)

#### For Self-managed Users
{:.no_toc}

To ensure that we can match you with your Self-managed license when opening a support ticket, please:
- use a company provided email address (no generic email addresses such as Gmail, Yahoo, etc.); AND
- provide GitLab with one of the following:
   - A screenshot of the license page (`/admin/license`)
   - The output of the command `sudo gitlab-rake gitlab:license:info`
   - The license ID displayed on the `/admin/license` page (GitLab 13.2+)
   - The license file provided to you at the time of purchase **and at least one of these:**
     - the email address to which the license was issued
     - the company name to which the license was issued

## On This Page
{:.no_toc}

- TOC
{:toc}

## First time reaching support?

Account setup can happen one of two ways:

1.  Go to [support.gitlab.com](https://support.gitlab.com/hc/en-us/requests/new)
    and submit a new request. An account and password will be created for you.
    You will need to request a password reset and setup a new password before
    you can sign in.
2.  Click on [Sign Up](https://gitlab.zendesk.com/auth/v2/login/registration?auth_origin=3252896%2Ctrue%2Ctrue&brand_id=3252896&return_to=https%3A%2F%2Fsupport.gitlab.com%2Fhc%2Fen-us&theme=hc) to create a new account using your company email address.

You can keep track of all of your tickets and their current status using the
GitLab support portal! We recommend using the support portal for a
superior experience managing your tickets. To learn more about using the
support portal, watch
[this video on using **Zendesk as an end user**](https://www.youtube.com/watch?v=NQzkGD7nIqQ).

Occasionaly, you may find the [support portal](https://support.gitlab.com) not
acting as expected. This is often caused by the user’s setup. When encountering
this, the recommended course of action is:

1. Ensure your browser is allowing third party cookies. These are often vital
   for the system to work. A general list to allow would be:
   * `[*.]zendesk.com`
   * `[*.]zdassets.com`
   * `[*.]gitlab.com`
1. Disable all plugins/extensions/addons on the browser.
1. Disable any themes on the browser.
1. Clear all cookies and cache on the browser.
1. Try logging in again to the the Support Portal.
1. If you are still having issues, write down the browser’s version, type,
   distro, and other identifying information.
1. Generate a HAR file (process will vary from browser to browser) and send
   this to support. If you are unable to create a ticket, then communicate with
   your Technical Account Manager and/or Account Manager. The next best place
   to send HAR file and browser information is via a GitLab.com issue.

### Language Support

Ticket support is available in the following languages:
- Chinese
- English
- French
- German
- Japanese
- Korean
- Portuguese
- Spanish

Should you be offered a call, only English is available.

NOTE:
Any attached media used for ticket resolution must be sent in English.

## GitLab Support Service Levels

### Trials Support

Trial licenses do not include support at any level. If part of your evaluation of GitLab includes evaluating support expertise or
SLA performance, please consider [contacting Sales](https://about.gitlab.com/sales/) to discuss options.

### Standard Support (Legacy)

Standard Support is included in Legacy GitLab self-managed **Starter**, and GitLab.com **Bronze** plans. It includes 'Next business day support' which means you can expect a reply to your ticket within 24 hours 24x5. (See [Support Staffing Hours](#definitions-of-gitlab-support-hours)).

Please submit your support request through the [support web form](https://support.gitlab.com/).
When you receive your license file, you will also receive an email address to use if
you need to reach Support and the web form can't be accessed for any reason.

### Priority Support

Priority Support is included with all self-managed and SaaS GitLab [Premium and Ultimate](/pricing/) purchases. These plans receive **Tiered Support response times**:

| [Support Impact](#definitions-of-support-impact) | First Response Time SLA  | Hours | How to Submit |
|-----------------|-------|------|------------------------------------------------|
| Emergency (Your GitLab instance is completely unusable) | 30 minutes | 24x7 | [Please trigger an emergency](#how-to-trigger-emergency-support) |
| Highly Degraded (Important features unavailable or extremely slow; No acceptable workaround) | 4 hrs | 24x5 | Please submit requests through the [support web form](https://support.gitlab.com/). |
| Medium Impact   | 8 hrs | 24x5 | Please submit requests through the [support web form](https://support.gitlab.com/). |
| Low Impact      | 24 hrs| 24x5 | Please submit requests through the [support web form](https://support.gitlab.com/). |

Self-managed Premium and Ultimate may also receive:

- **Support for Scaled Architecture**: A Support Engineer will work with your technical team around any issues encountered after an implementation of a scaled architecture is completed in cooperation with our Customer Success team.
- **Live upgrade assistance**: Request to schedule an upgrade time with GitLab to move from GitLab version to updated version. We'll host a live screen share session to help you through the process and ensure there aren't any surprises. Learn [how to schedule an upgrade.](scheduling-live-upgrade-assistance.html#how-do-i-schedule-live-upgrade-assistance)

#### Definitions of Support Impact

- **Low** - Questions or Clarifications around features or documentation or deployments (24 hours)
  *Minimal or no Business Impact.* Information, an enhancement, or documentation clarification is requested, but there is no impact on the operation of GitLab. Implementation or production use of GitLab is continuing and work is not impeded. Example: A question about enabling ElasticSearch.
- **Medium** - Something is preventing normal GitLab operation (8 hours)
  *Some Business Impact.* Important GitLab features are unavailable or somewhat slowed, but a workaround is available. GitLab use has a minor loss of operational functionality, regardless of the environment or usage. Example: A known bug impacts the use of GitLab, but a workaround is successfully being used as a temporary solution.
- **High** - GitLab is Highly Degraded (4 hours)
  *Significant Business Impact.* Important GitLab features are unavailable or extremely slowed, with no acceptable workaround. Implementation or production use of GitLab is continuing; however, there is a serious impact on productivity. Example: CI Builds are erroring and not completing successfully, and the software release process is significantly affected.
- **Emergency** - Your instance of GitLab is unavailable or completely unusable (30 Minutes)
  *A GitLab server or cluster in production is not available, or is otherwise unusable.* An emergency ticket can be filed and our On-Call Support Engineer will respond within 30 minutes. Example: GitLab showing 502 errors for all users.
  
##### How to Trigger Emergency Support

To trigger emergency support you **must send a new email** to the emergency contact address provided with your GitLab license. When your license file was sent to your licensee email address, GitLab also sent a set of addresses to reach Support for emergency requests. You can also ask your Technical Account Manager or sales rep for these addresses.

- **Note:** CCing the address on an existing ticket or forwarding it will **not** page the on-call engineer.

It is preferable to [include any relevant screenshots/logs in the initial email](#working-effectively-in-support-tickets). However if you already have an open ticket that has since evolved into an emergency, please include the relevant ticket number in the initial email.

 - **Note:** For GitLab.com customers our infrastructure team is on-call 24/7 - please check [status.gitlab.com](https://status.gitlab.com) before contacting Support.

Once an emergency has been resolved, GitLab Support will close the emergency ticket. If a follow up is required post emergency, GitLab Support will either continue the conversation via a new regular ticket created on the customer's behalf, or via an existing related ticket.

#### Definition of Scaled Architecture
Scaled architecture is defined as any GitLab installation that separates services for the purposes of resilience, redundancy or scale. As a guide, our [2,000 User Reference Architecture](https://docs.gitlab.com/ee/administration/reference_architectures/2k_users.html) (and higher) would fall under this category.

[Priority Support](#priority-support) is required to receive assistance in troubleshooting a scaled implementation of GitLab.

{:.no_toc}
##### Scaled Architecture - Omnibus and Source Installations
In Omnibus and Source installations, Scaled Architecture is any deployment with multiple GitLab application nodes. This does not include external services such as Amazon RDS or ElastiCache.

|  **Service** | **Scaled Architecture** | **Not Scaled Architecture** |
| --- | --- | --- |
|  Application (GitLab Rails) | Using multiple application nodes to provide resillience, scalability or availability | Using a single application node |
|  Database | Using multiple database servers with Consul and PgBouncer | Using a single separate database or managed database service |
|  Caching | Using Redis HA across multiple servers with Sentinel | Using a single separate server for Redis or a managed Redis service |
|  Repository / Object Storage | Using one or more separate Gitaly nodes. Using NFS across multiple application servers. | Storing objects in S3 |
|  Job Processing | Using one or more separate Sidekiq nodes | Using Sidekiq on the same host as a single application node |
|  Load Balancing | Using a Load Balancer to balance connections between multiple application nodes | Using a single application node |
|  Monitoring | Not considered in the definition of Scaled Architecture | Not considered in the definition of Scaled Architecture |

#### Service Level Agreement (SLA) details

- GitLab offers 24x5 support (24x7 for Priority Support Emergency tickets) bound by the SLA times listed above.
- The SLA times listed are the time frames in which you can expect the first response.
- GitLab Support will make a best effort to resolve any issues to your satisfaction as quickly as possible. However, the SLA times are *not* to be considered as an expected time-to-resolution.

#### Definitions of GitLab Support Hours

- **24x5** - GitLab Support Engineers are actively responding to tickets Sunday 3pm Pacific Time through Friday 5pm Pacific Time.
- **24x7** - For [Emergency Support](#definitions-of-support-impact) there is an engineer on-call 24 hours a day, 7 days a week.

These hours are the SLA times when selecting 'All Regions' for 'Preferred Region for Support'.

#### Effect on Support Hours if a preferred region for support is chosen

When submitting a new ticket, you will select a 'preferred region for support'. This helps us assign Support Engineers from your region and means you'll be more likely to receive replies in your business hours (rather than at night).
If you choose a preferred region, the Support Hours for the purposes of your ticket SLA are as follows:

- Asia Pacific (APAC): 09:00 to 21:00 AEST (Brisbane), Monday to Friday
- Europe, Middle East, Africa (EMEA): 08:00 to 18:00 CET Amsterdam, Monday to Friday
- Americas (AMER): 05:00 to 17:00 PT (US & Canada), Monday to Friday

Customers who select 'All regions' as their preferred region will receive SLAs of 24x5 as described above.

### Customer Satisfaction
24 hours after a ticket is Solved or automatically closed due to a lack of activity, a Customer Satisfaction survey will be sent out.
We track responses to these surveys through Zendesk with a target of 95% customer satisfaction.

Support Management regularly reviews responses, and may contact customers who leave negative reviews for more context.

### Phone and video call support
GitLab does not offer support via inbound or on-demand calls.

GitLab Support Engineers communicate with you about your tickets primarily
through updates in the tickets themselves. At times it may be
useful and important to conduct a call, video call, or screensharing session
with you to improve the progress of a ticket. The support engineer may suggest
a call for that reason. You may also request a call if you feel one is needed.
Either way, the decision to conduct a call always rests with the support 
engineer, who will determine:

* whether a call is necessary; and
* whether we have sufficient information for a successful call.

Once the decision has been made to schedule a call, the support engineer will:

1. Send you a link (through the ticket) to our scheduling platform or, in the
   case of an emergency, a direct link to start the call.
1. Update you through the ticket with: (a) an agenda and purpose for the call,
   (b) a list of any actions that must be taken to prepare for the call, and
   (c) the maximum time allowed for the call. _Please expect that the call
   will end as soon as the stated purpose has been achieved or the time limit
   has been reached, whichever occurs first._

**NOTE:** Calls scheduled by GitLab Suport are on the Zoom platform.
If you cannot use Zoom, you can request a Cisco Webex link. If neither of these
work for you, GitLab Support can join a call on the following platforms: 
Microsoft Teams, Google Hangouts, Zoom, Cisco Webex. Other video platforms 
are not supported.

***Please Note:***  Attempts to reuse a previously-provided scheduling link to 
arrange an on-demand call will be considered an abuse of support, and will 
result in such calls being cancelled.

## General Support Practices

### Differences Between Support Tickets and GitLab Issues

It's useful to know the difference between a support ticket opened through our [support portal](https://support.gitlab.com) vs. [an issue on GitLab.com](https://gitlab.com/gitlab-org/gitlab/-/issues).

For customers with a license or subscription that includes support, always feel free to contact support with your issue in the first instance via [the support portal](https://support.gitlab.com). This is the primary channel the support team uses to interact with customers and it is the only channel that is based on an SLA. Here, the GitLab support team will gather the necessary information and help debug the issue. In many cases, we can find a resolution without requiring input from the development team. However, sometimes debugging will uncover a bug in GitLab itself or that some new feature or enhancement is necessary for your use-case.

This is when we create an [issue on GitLab.com](https://gitlab.com/gitlab-org/gitlab/-/issues) - whenever input is needed from developers to investigate an issue further, to fix a bug, or to consider a feature request. In most cases, the support team can create these issues on your behalf and assign the correct labels. Sometimes we ask the customer to create these issues when they might have more knowledge of the issue or the ability to convey the requirements more clearly. Once the issue is created on GitLab.com it is up to the product managers and engineering managers to prioritize and drive the fix or feature to completion. The support team can only advocate on behalf of customers to reconsider a priority. Our involvement from this point on is more minimal.

#### We don't keep tickets open (even if the underlying issue isn't resolved)

Once an issue is handed off to the development team through an issue in a GitLab tracker, the support team will close out the support ticket as Solved _even if the underlying issue is not resolved_. This ensures that issues remain the single channel for communication: customers, developers and support staff can communicate through only one medium.

### Issue Creation

Building on the above section, when bugs, regressions, or any application behaviors/actions **not working as intended** are reported or discovered during support interactions, the GitLab Support Team will
create issues in GitLab project repositories on behalf of our customers.

For feature requests, both involving the addition of new features as well as the change of
features currently **working as intended**, support will request that the customer
create the issue on their own in the appropriate project repos.

### Working Effectively in Support Tickets

As best you can, please help the support team by communicating the issue you're facing,
or question you're asking, with as much detail as available to you. Whenever possible,
include:
- [log files relevant](https://docs.gitlab.com/ee/administration/logs.html#gathering-logs) to the situation.
- steps that have already been taken towards resolution.
- relevant environmental details, such as the architecture.

We expect for non-emergency tickets that GitLab administrators will take 20-30 minutes to formulate the support ticket with relevant information. A ticket without the above information will reduce the efficacy of support.

In subsequent replies, the support team may ask you follow-up questions. Please
do your best read through the entirety of the reply and answer any such questions.
If there are any additional troubleshooting steps, or requests for additional
information please do your best to provide it.

The more information the team is equipped with in each reply will result
in faster resolution. For example, if a support engineer has to ask for logs,
it will result in more cycles. If a ticket comes in with everything required,
multiple engineers will be able to analyze the problem and will have what is
necessary to further escalate to developers if so required.

#### Please don't use language intended to threaten or harass
If your ticket contains language that goes against the [GitLab Community Code of Conduct](/community/contribute/code-of-conduct/), you'll receive the following response and have your ticket closed:


>Hello,
>
>While we would usually be very happy to help you out with any issue, we cannot assist you on this ticket due to the language used not adhering to the [GitLab Community Code of Conduct](https://about.gitlab.com/community/contribute/code-of-conduct/). As noted in our [Statement of Support](https://about.gitlab.com/support/#please-dont-use-language-intended-to-threaten-or-harass), we're closing this ticket.
>
>Please create a new ticket that complies with our guidelines and one of our support engineers will be happy to assist you.
>
>Thank you,


Repeat violations may result in termination of your support contract and/or business relationship with GitLab.


#### Please don't send encrypted messages
Some organizations use 3rd party services to encrypt or automatically expire messages. As a matter of security, please do not include
any sensitive information in the body or attachments in tickets. All interactions with GitLab Support should be in plain-text.

GitLab Support Engineers will not click on URLs contained within tickets or interact with these type of 3rd party services to provide support.
If you do send such a reply, an engineer will politely ask you to submit your support ticket in plain text, and link you to this section of our Statement of Support.

If there's a pressing reason for which you need to send along an encrypted file or communication, please discuss it with the Engineer in the ticket.

#### Please don't share login credentials
Do not share login credentials for your GitLab instance to the support team. If the team needs more information about the problem, we will offer to schedule a
call with you.

### Sanitizing data attached to Support Tickets

If relevant to the problem and helpful in troubleshooting, a GitLab Support Engineer will request information regarding configuration files or logs.

We encourage customers to sanitize all secrets and private information before sharing them in a GitLab Support ticket.

Sensitive items that should never be shared include:

- credentials
- passwords
- tokens
- keys
- secrets

There's more specific information on the dedicated [handling sensitive information with GitLab Support](sensitive-information.html) page.

### Support for GitLab on restricted or offline networks

GitLab Support may request logs in Support tickets or ask you to screenshare in customer calls if it would be the most efficient and effective way to troubleshoot and solve the problem.

Under certain circumstances, sharing logs or screen sharing may be difficult or impossible due to our customers' internal network security policies.

GitLab Support will never ask our customers to violate their internal security policies, but Support Engineers do not know the details of our customers' internal network security policies.

In situations where internal or network security policies would prevent you from sharing logs or screen sharing with GitLab Support, please communicate this as early as possible in the ticket so we can adjust the workflows and methods we use to troubleshoot.

Customer policies preventing the sharing of log or configuration information with Support may lengthen the time to resolution for tickets.

Customer policies preventing screen sharing during GitLab Support customer calls may impact a Support Engineer's ability to resolve issues during a scheduled call.

### Including colleagues in a support ticket

Zendesk will automatically drop any CCed email addresses for tickets that come in via email.
If you would like to include colleagues in a support interaction, there are two ways:

1. Ask in the ticket to have any number of addresses CCed. This will last for the duration of the ticket,
but if you open a new ticket you'll need to request additional participants be added again.
1. Ask your sales representative or support team in a ticket to submit a request on your behalf to allow others in your organization to view or comment on open support cases.

To view tickets you have been CCed on, navigate to **Profile Icon > My activities > Requests I'm CC'd on**.

### Solving Tickets

If a customer explains that they are satisfied their concern is being addressed
properly in an issue created on their behalf, then the conversation should continue
within the issue itself, and GitLab support will close the support ticket. Should
a customer wish to reopen a support ticket, they can simply reply to it and it will
automatically be reopened.

##### Can Users Solve Tickets Themselves?
A user can solve a ticket themselves by logging into the Support Portal and navigating to the [My activities](https://support.gitlab.com/hc/en-us/requests) page. From there the user can select one of their tickets that is in an open or pending state, navigate to the bottom of the ticket notes section, check the `Please consider this request solved` box and press `Submit`. If the user would like to reopen the case they can simply respond with a new comment and the case will reopen or create a follow up case.

### GitLab Instance Migration

If a customer requests assistance in migrating their existing self-hosted GitLab to
a new instance, you can direct them to our [Migrating between two self-hosted GitLab
Instances](https://docs.gitlab.com/ee/user/project/import/#migrating-between-two-self-hosted-gitlab-instances)
documentation. Support will assist with any issues that arise from the GitLab migration.
However, the setup and configuration of the new instance, outside of GitLab specific configuration,
is considered out of scope and Support will not be able to assist with any resulting issues.


### Handling Unresponsive Tickets

To prevent an accumulation of tickets for which we have not received a response within a specific timescale, we have established the following process.
If the ticket owner (our customer) fails to respond within 20 days following our last reply, our ticketing system will mark the ticket as solved.
If the ticket owner (our customer) fails to respond within 7 days of the ticket being marked as solved, our ticketing system will proceed to close the ticket.

## GitLab.com Specific Support Policies

### Account Recovery and 2FA Resets

If you are unable to sign into your account we can help you regain access.
**This service is only available for paid users** (you are part of paid group or paid user namespace).

**Forgotten password?**

1. Use the [Reset password form](https://gitlab.com/users/password/new)
1. If you don't receive the reset email please [contact support](https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360000803379)

**Locked out by 2FA?**

Are you on a free plan? [Please read this blog post](https://about.gitlab.com/blog/2020/08/04/gitlab-support-no-longer-processing-mfa-resets-for-free-users/)

1. Try to [generate new recovery keys using SSH](https://docs.gitlab.com/ee/user/profile/account/two_factor_authentication.html#generate-new-recovery-codes-using-ssh)
1. Paid users only: If you are still unable to sign in after trying step one, [contact support](https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360000803379). We will request evidence to prove account ownership. We cannot guarantee to recover your account unless you pass the verification checks.

Please note that in some cases reclaiming an account may be impossible. Read
["How to keep your GitLab account safe"](/blog/2018/08/09/keeping-your-account-safe/)
for advice on preventing this.

### Restoration of Deleted Data

Any type of data restoration is currently a manual and time consuming process lead by GitLab’s infrastructure team. [Our infrastructure team clearly states](https://about.gitlab.com/handbook/engineering/infrastructure/faq/#q-if-a-customer-project-is-deleted-can-it-be-restored) that “once a project is deleted it cannot be restored”.

We encourage customers to use:

1. the [delayed deletion](https://docs.gitlab.com/ee/user/group/#enabling-delayed-project-removal) feature if it is available in your plan,
1. [export projects and/or groups](https://docs.gitlab.com/ee/user/project/settings/import_export.html) regularly, particularly via the API.

GitLab will consider restoration requests only when the request is for a project or group that is part of a **paid plan** with an active subscription applied, and one of the following is true:

- The data was deleted due to a GitLab bug.
- The organization’s contract includes a specific provision.

Please note that user accounts and individual contributions cannot be restored.

### Ownership Disputes

GitLab will not act as an arbitrator of Group or Account ownership disputes. Each user and group owner is responsible for ensuring that they are following best practices for data security.

As GitLab subscriptions are generally business-to-business transactions, in the event that a former employee has revoked company access to a paid group, please contact GitLab Support for recovery options.

### Name Squatting Policy

Per the [GitLab Terms of Service](/terms):

> Account name squatting is prohibited by GitLab. Account names on GitLab are administered to users on a first-come, first-serve basis. Accordingly, account names cannot be held or remain inactive for future use.

The GitLab.com Support Team will consider a [namespace](https://docs.gitlab.com/ee/user/group/#namespaces) (user name or group name) to fall under the provisions of this policy when the user has not logged in or otherwise used the namespace for an extended time.

Specifically:

- User namespaces can be reassigned if **both** of the following are true:

   1. The user's last sign in was at least two years ago.
   2. The user is not the sole owner of any active projects.

- Group namespaces can be reassigned if **one** of the following is true:

   1. There is no data (no project or project(s) are empty).
   2. The owner's last sign in was at least two years ago.

If the namespace contains data, GitLab Support will attempt to contact the owner over a two week period before reassigning the namespaces. If the namespace contains no data (empty or no projects) and the owner is inactive, the namespace will be released immediately.

Namespaces associated with unconfirmed accounts over 90 days old are eligible for immediate release. Group namespaces that contain no data and were created more than 6 months ago are likewise eligible for immediate release.

### Namespace & Trademarks

GitLab.com namespaces are available on a first come, first served basis and cannot be reserved. No brand, company, entity, or persons own the rights to any namespace on GitLab.com and may not claim them based on the trademark. Owning the brand "GreatCompany" does not mean owning the namespace "gitlab.com/GreatCompany". Any dispute regarding namespaces and trademarks must be resolved by the parties involved. GitLab Support will never act as arbitrators or intermediaries in these disputes and will not take any action without the appropriate legal orders.

### Log requests

Due to our [terms](/terms), GitLab Support cannot provide raw copies of logs. However, if users have concerns, Support can answer specific questions and provide summarized information related to the content of log files.

For paid users on GitLab.com, many actions are logged in the [Audit events](https://docs.gitlab.com/ee/administration/audit_events.html#group-events) section of your GitLab.com project or group.

## US Federal Support

US Federal Support is built for companies and organizations that require that only US Citizens have access to the data contained within their support issues.
The unique requirements of US Federal Support result in the following specific
policies:

### Limitations

To be eligible to utilize this instance, your account manager must approve and you must meet one of the following criteria:

1. Be using a Premium or Ultimate License
1. Be using a Starter license with 2000 or more license seats

For more information about utilizing this method of support, please contact your
Account Manager.

### Hours of operation

Due to the requirement that support be provided only by US citizens, US Federal Support
hours of operation are limited to:

* Monday through Friday, 0500-1700 Pacific Time

### US Federal Emergency support

The US Federal instance offers emergency support. However, this is limited to
the hours of 0500 to 1700 Pacific Time 7 days a week. You should receive
instructions for triggering emergency support from your Account Manager.

### CCs are disabled
To help ensure that non-US citizens are not inadvertently included in a support
interaction, the "CC" feature is disabled. As a result, support personnel will
be unable to add additional contacts to support tickets.

By request through your account manager, you may allow certain individuals in
your organization the ability to view and respond to any open support tickets
through the US Federal Support Portal.

## Further resources

Additional resources for getting help, reporting issues, requesting features, and
so forth are listed on our [get help page](/get-help/).

## How is Support doing?
In the spirit of ["Is it any good?"](/is-it-any-good/) and GitLab's Value of [Transparency](/handbook/values/#transparency)
GitLab Support publishes its performance indicators publicly.

- [Customer Support Performance Indicators](/handbook/support/performance-indicators/)
   - [Satisfaction with Support](/handbook/support/performance-indicators/#support-satisfaction-ssat)
   - [SLA Achievement](/handbook/support/performance-indicators/#service-level-agreement-sla)


