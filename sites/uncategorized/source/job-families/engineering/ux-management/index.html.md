---
layout: job_family_page
title: "UX Management"
description: "Managers in the UX department at GitLab see the team as their product. While they are credible as designers and know the details of what Product Designers work on, their time is spent hiring a world-class team and putting them in the best position to succeed."
---

# UX Management Roles at GitLab

Managers in the UX department at GitLab see the team as their product. While they are credible as designers and know the details of what Product Designers work on, their time is spent hiring a world-class team and putting them in the best position to succeed. They own the delivery of UX commitments and are always looking to improve productivity. They must also coordinate across departments to accomplish collaborative goals.

## Product Design Manager

The Product Design Manager reports to the Senior Product Design Manager or the Director of Product Design, and Product Designers report to the Product Design Manager.

### Job Grade

The Product Design Manager is a [grade 8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Responsibilities

* **Product knowledge:** Understand the technology and features of the stages or areas you are assigned and have working knowledge of the end-to-end GitLab product.
* **Cross-product collaboration:** Proactively identify large, strategic UX opportunities that span the stages or areas you are responsible for and the product as a whole. Work with other Product Design Managers, Staff, and Principal Product Designers to drive cross-product initiatives.
* **Design quality:** Review UX deliverables (research, designs, etc.) that your team creates, and provide feedback to ensure high-quality output.
* **Research:** Identify strategic user research initiatives that span multiple stage groups (and possibly the entire product), and work with other Product Design/Research Managers to organize research efforts.
* **UX evangelism:** Communicate the value of UX to cross-functional GitLab team members and work with them to leverage the [Product Development Flow](/handbook/product-development-flow/).
* **UX debt:** Help the product teams of stages or areas you support to prioritize UX initiatives including [UX debt](/handbook/engineering/ux/performance-indicators/#ux-debt) and overall usability improvements to the product.
* **UX process**: Set up and manage collaborative processes within your team to ensure Product Designers, Technical Writers, and Researchers are actively working together. Make sure everyone has exposure to the work that is happening within the broader team. Collaborate with your peers to make improvements to how we work across the design organization.
* **Hiring:** Interview and conduct portfolio reviews of Product Design candidates to hire a world-class team. Review and provide feedback on interview scorecards to maintain a high-quality interview process. 
* **Public presence:** Help promote GitLab publicly through activities like writing blog articles, giving talks, publishing videos to GitLab Unfiltered, and engaging in social media efforts, where appropriate.
* **Vision and direction:** Have an awareness of [Opportunity Canvas](/handbook/product/product-processes/#opportunity-canvas) reviews, strategy, and vision of the stages or areas you're assigned.
* **People management:** Hold weekly [1:1s](/handbook/leadership/1-1/) with every member of your team and create Individual Growth Plans with monthly Career Development check-ins.

### Specialties

Read more about what a [specialty](/handbook/hiring/vacancies/#definitions) is at GitLab here.

#### FE/UX Foundations

The Foundations team works on building a cohesive and consistent user experience, both visually and functionally. You'll be responsible for leading the direction of the experience design, visual style, and technical tooling of the GitLab product. You'll act as a centralized resource, helping to triage large-scale experience problems as the need arises.

You'll spend your time collaborating with a [cross-functional team](/handbook/product/categories/#ecosystem-group), helping to implement our [Design System](https://design.gitlab.com/), building comprehensive accessibility standards into our workflows, and defining guidelines and best practices that will inform how teams design and build products. A breakdown of the vision you’ll help to deliver within the FE/UX Foundation category can be found on our [product direction page](/direction/create/ecosystem/frontend-ux-foundations/).

### Requirements

* A minimum of 3 years experience managing a group of designers.
* Solid visual awareness with understanding of basic design principles like typography, layout, composition, and color theory.
* Proficiency with pre-visualization software (e.g. Figma, Sketch, Adobe Photoshop, Illustrator).
* Experience defining the high-level strategy (the why) and creating design deliverables (the how) based on research.
* Experience driving organizational change with cross-functional stakeholders.
* Passion for creating visually pleasing and intuitive user experiences.
* Collaborative team spirit with great communication skills.
* You share our [values](/handbook/values/), and work in accordance with those values.
* [Leadership at GitLab](/company/team/structure/#management-group).
* Ability to use GitLab.

**NOTE** In the compensation calculator below, fill in "Manager" in the `Level` field for this role.

### Interview Process

* [Screening call](/handbook/hiring/#screening-call) with a recruiter.
* **Interview with Product Designer** In this interview, the interviewer will want to understand the experience you have as a manager, what type of teams you have led, and your management style. The interviewer will also look to understand how you define strategy, how you work with researchers, how you handle conflict, and how you dealt with difficult situations in the past. Do be prepared to talk about your work, your experience with Design Systems, and your technical ability.
* **Interview with Product Design Manager** In this interview, we want you to share specific examples from your work that provide insight into a problem you solved as part of a project you led. We'll look to understand the size and structure of your team, the goals of the project, how you/the team approached research, how you synthesized research data to inform design decisions, what design standards and guidelines you worked within, how you collaborated with the wider team, and the overall outcome. Broadly, we want to hear how you identified what needed to be done and then guided your team to the end result. A formal case study is not required but welcomed.
* **Interview with Director of Product Design** In this interview, the interviewer will want to understand the experience you have as a manager, what types of teams you have led, and your management style. The interviewer will also want to understand how you define strategy, how you work with researchers, how you handle conflict, and how you've dealt with difficult situations in the past. Do be prepared to talk about your work, experience with Design Systems, and your technical ability.
* **Interview with Director of Product (Product Management)** In this interview, the interviewer will want to understand how your career experiences will set you up for success at GitLab. They will also look to understand how you work with cross-functional partners, the domains you've worked in previously, and the types of teams you've led. 
* **Interview with VP of UX** In this interview, the interviewer will want to understand the experience you have as a manager, your experience working remotely, and how these two elements of your career intersect. They will also look to understand your technical skills and the types of products you've worked on previously.  

## Senior Product Design Manager

The Senior Product Design Manager reports to the Director of Product Design, and the Product Design Manager reports to the Senior Product Design Manager.

### Job Grade

The Senior Product Design Manager is a [grade 9](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Responsibilities

* **Performance tracking:** Define and manage performance indicators for the Product Design team by actively contributing to the product design KPIs on the [UX KPIs](/handbook/engineering/ux/performance-indicators/) page in the handbook.
* **Cross-product collaboration:** Actively advocate for Product Design throughout the organization by ensuring Product Design responsibilities are reflected in the Product Development Flow.
* **Product knowledge:** Help drive cross-product workflows by having an awareness of what's happening across all sections through active participation in design reviews, UX Showcases, and Group Conversations.
* **Goal setting:** Facilitate the creation and execution of product design [OKRs](/company/okrs/) in collaboration with the Product Design team and UX Leadership.
* **UX evangelism:** Ensure UX is prioritized by working with product leadership to identify opportunities for validation and better cross-functional collaboration.
* **People management:** Coach Product Design Managers on how to conduct 1:1s and growth conversations with their direct reports.
* **Skip levels:** Conduct quarterly skip levels with your reports' direct reports.
* **Vision and direction**: Have an awareness of Opportunity Canvas reviews, strategy, and vision of the stages you're assigned.
* **Hiring:** Hire and retain a world-class team of Product Designers and Product Design Managers.

### Requirements

* A minimum of 3 years experience managing Product Design Managers.
* Solid visual awareness with understanding of basic design principles like typography, layout, composition, and color theory.
* Experience defining the high-level strategy (the why) and helping your team tie design and research back to that strategy.
* Experience driving organizational change with cross-functional stakeholders.
* Passion for creating visually pleasing and intuitive user experiences.
* Collaborative team spirit with great communication skills.
* You share our [values](/handbook/values/), and work in accordance with those values.
* [Leadership at GitLab](/company/team/structure/#management-group).
* Ability to use GitLab.

**NOTE** In the compensation calculator below, fill in "Manager" in the `Level` field for this role.

### Interview Process

* [Screening call](/handbook/hiring/#screening-call) with a recruiter.
* Interview with Manager, Product Design. In this interview, the interviewer will focus on understanding your experience with driving design strategy, managing managers, and influencing the wider organization in which you worked. Examples of large, complex projects that had a significant impact on product experience will be helpful. Broadly, we want to hear how you identified what needed to be done and then guided your team to the end result.
* Interview with Director of Product Design. In this interview, the interviewer will spend a lot of time trying to understand the experience you have as a manager, what types of teams you have led and your management style. The interviewer will also want to understand how you define strategy, how you work with researchers, how you've handled conflict, and how you've dealt with difficult situations in the past. Do be prepared to talk about your work, experience with Design Systems, and technical ability, too.
* Interview with Director of Product (Product Management).
* Interview with VP of Engineering.

## Director of Product Design

The Director of Product Design reports to the VP of UX, and Product Design Manager and Senior Product Design Manager report to the Director of Product Design.

The Director of Product Design role extends the Senior Product Design Manager role.

### Job Grade

The Director of Product Design is a [grade 10](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Responsibilities

* **Performance tracking:** Define and manage performance indicators for the Product Design team by independently managing product design KPIs on the [UX KPIs](/handbook/engineering/ux/performance-indicators/) page in the handbook.
* **Cross-product collaboration:** Actively advocate for Product Design throughout the organization by ensuring Product Design responsibilities are reflected in the Product Development Flow.
* **Product knowledge:** Help drive cross-product workflows by having an awareness of what's happening across all sections through active participation in design reviews, UX Showcases, and Group Conversations.
* **Goal setting:** Independently manage the creation and execution of product design [OKRs](/company/okrs/) with feedback from the Product Design team and UX Leadership.
* **UX evangelism:** Ensure UX is prioritized by working with product leadership to identify opportunities for validation and better cross-functional collaboration.
* **Design strategy:** Communicate significant product design strategy decisions to leadership and the wider company.
* **People management:** Coach Product Design Managers on how to conduct 1:1s and growth conversations with their direct reports.
* **Skip levels:** Conduct quarterly skip levels with your reports' direct reports.
* **Vision and direction**: Have an awareness of [Opportunity Canvas](/handbook/product/product-processes/#opportunity-canvas) reviews, strategy, and visions across the product. 
* **Hiring:** Hire and retain a world-class team of Product Designers and Product Design Managers.

### Requirements

* A minimum of 10 years experience managing designers, and leading design for a product company.
* Solid visual awareness with understanding of basic design principles like typography, layout, composition, and color theory.
* Proficiency with pre-visualization software (e.g. Figma, Sketch, Adobe Photoshop, Illustrator).
* Experience defining the high-level strategy (the why) and creating design deliverables (the how) based on research.
* Passion for creating visually pleasing and intuitive user experiences.
* Collaborative team spirit with great communication skills.
* You share our [values](/handbook/values/), and work in accordance with those values.
- [Leadership at GitLab](/company/team/structure/#director-group).

### Interview Process

* [Screening call](/handbook/hiring/#screening-call) with a recruiter.
* Interview with Manager, Product Design or Director of Product Design. In this interview, the interviewer will spend a lot of time trying to understand the experience you have as a manager, as well as what type of teams you have led and your management style. The interviewer will also be looking to understand how you define strategy, how you work with researchers, how you've handled conflict, and how you dealt with difficult situations in the past. Do be prepared to talk about your work, experience with Design Systems, and technical ability, too.
* Interview with Director of Product (Product Management).
* Interview with VP of UX.

## Vice President of UX

The VP of UX (User Experience) reports to the Chief Technology Officer (CTO), and Senior Managers and Directors of Product Design, UX Research, and Technical Writing report to the VP of UX. 

### Job Grade

The VP of UX is a [grade 12](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Responsibilities

* **UX strategy:** Set a UX vision that aligns Product Design, UX Research, and Technical Writing with overall company objectives.
* Communicate broadly about important UX initiatives, setting an ambitious UX vision for the department, product, and company.
* **UX resources:** Manage the UX budget, including compensation planning, non-headcount budget allocation, and tradeoff decisions.
* **Executive collaboration:** Interface regularly with executives on important decisions.
* **UX initiatives:** Identify ways to elevate the GitLab product experience, manage initiatives to address those concerns, and track and communicate about progress.
* **Coach UX leaders:** Help UX leaders grow their skills and leadership experience.
* **Culture:** Foster an open and collaborative culture based on trust in the UX department, where everyone feels empowered to do their best work.
* **Cross-product collaboration:** Ensure that UX is well-integrated into the [Product Development Flow](/handbook/product-development-flow/), and advocate for process changes that help product management, engineering, and UX work together to build a great experience.
* **Design system:** Define and promote design guidelines, best practices, and standards, and help to drive GitLab's [design system](https://design.gitlab.com/) forward at a strategic level.
* **Research evangelism:** Work with product leadership to prioritize research efforts, so that we validate whether we're solving the right problems in the right ways.
* **Democratize UX:** Ensure that Development is included in the UX process by offering the opportunity to participate in and understand the outcomes of user research, give early feedback on upcoming designs, and participate in design system strategy.
* **Goal setting:** Define value-driven quarterly UX OKRs, and manage their execution.
* **Public presence:** Represent the company at conferences, in media, in blog articles, in YouTube videos, and in other public venues.
* **Skip levels:** Hold regular skip-level 1:1s with all members of their team.
* **Team building:** Hire and retain a world-class team of Product Designers, UX Researchers, Technical Writers, and their managers.

### Requirements

* A minimum of 10 years experience managing designers, researchers, and writers and leading design for a product company.
* Solid visual awareness with understanding of basic design principles like typography, layout, composition, and color theory.
* Passion for creating visually pleasing and intuitive user experiences.
* Collaborative team spirit with great communication skills.
* You share our [values](/handbook/values/), and work in accordance with those values.
- [Leadership at GitLab](https://about.gitlab.com/company/team/structure/#director-group)

### Interview Process

* [Screening call](/handbook/hiring/#screening-call) with a recruiter.
* Interview with a manager on the UX Leadership team. In this interview, the interviewer will spend a lot of time trying to understand the experience you have leading managers, as well as what type of teams you have led and your management style. The interviewer will also be looking to understand how you define strategy, how you've handled conflict, and how you dealt with difficult situations in the past. Do be prepared to talk about your work, experience with Design Systems, and technical ability, too.
* Interview with a director on the UX Leadership team. In this interview, we will be looking for you to give some real insight into a problem you were solving as part of a large initiative you led the work on. We'll look to understand the size and structure of the team, the goals of the project, how you/the team approached research, how you used research to inform decisions, and how you collaborated with the wider team. Broadly, we want to hear how you identified what needed to be done and then guided your team to the end result.
* Interview with VP of Product.
* Interview with CTO.

As always, interviews and the screening call will be conducted via video.

See more details about our hiring process on the [hiring handbook](/handbook/hiring/).

## Performance indicators

* [Hiring Actual vs Plan](/handbook/engineering/ux/performance-indicators/#hiring-actual-vs-plan)
* [Diversity](/handbook/engineering/ux/performance-indicators/#diversity)
* [Handbook Update Frequency](/handbook/engineering/ux/performance-indicators/#handbook-update-frequency)
* [Team Member Retention](/handbook/engineering/ux/performance-indicators/#team-member-retention)

## Career Ladder

For more details on the engineering career ladders, please review the [engineering career development](/handbook/engineering/career-development/#roles) handbook page.
