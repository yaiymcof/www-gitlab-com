---
layout: markdown_page
title: "Procurement Guide: Collaborating with GitLab Legal"
---
## Overview
Thank you for visiting! The purpose of this resource is to provide GitLab team members with information on how legal assists and interacts with the procurement of products and services at GitLab. 

For information on the Procurement Team, policies and process, visit [The Procurement Page](https://about.gitlab.com/handbook/finance/procurement/)

For general questions that **_do not_** require legal advice, deliverables, or any discussion of confidential information, you can reach out to the GitLab Legal Team at *[`#legal`](https://gitlab.slack.com/archives/legal)*.

## Contacting Legal
You don't need to reach out to GitLab Legal directly, instead the GitLab Legal Team will be enaged directly by the Procurement Team, as applicable. 

## When is Legal involved
GitLab Legal will review any and all purchases made to ensure adequate terms are present for GitLab. Examples of types of purchases include:
- Software Agreements;
- Professional Services Agreements;
- Sponsorship Agreements;
- Event Contract;
- Subcontracting Agreements (staff augmentation or providing services/resources to GitLab and/or GitLab customers)

## Signing Contracts 
**DO NOT SIGN ANY CONTRACTS**

- Only authorized individuals can execute contracts on behalf of GitLab. Please view the [Signatory Matrix](https://about.gitlab.com/handbook/finance/authorization-matrix/#authorization-matrix) for who may sign.  

- In order to be executed, all Contracts must include the GitLab Legal stamp. This stamp confirms that the Contract has been reviewed and approved by a Legal Team Member. If you do not receive a GitLab Legal stamped version of the Contract, please ask the Procurement Team Member for assistance. 

## Vendor Requirements
- All vendors must agree to comply and act in accordance with:
    - [GitLab Code of Business Conduct and Ethics](https://about.gitlab.com/handbook/legal/gitlab-code-of-business-conduct-and-ethics/)
    - [GitLab Moder Slavery Act Transparency Statement](https://about.gitlab.com/handbook/legal/modern-slavery-act-transparency-statement/)
    - In order to use GitLab Name and Logo [Brand Guidelines Page](https://about.gitlab.com/handbook/marketing/corporate-marketing/brand-activation/trademark-guidelines/)


## Additional Requests & Information


NDA Request(s): [NDA Process](https://about.gitlab.com/handbook/legal/procurement-guide-collaborating-with-gitlab-legal/#nda-process)

Negotiating Terms and Conditions: [Negotiating Terms](https://about.gitlab.com/handbook/legal/procurement-guide-collaborating-with-gitlab-legal/#negotiating-terms)

## NDA Process
- Prior to exchanging any confidential information, GitLab and a potential Vendor should execute a Mutual Non-Disclosure Agreement. This will ensure the adequate protection of any / all information shared. 
- Update the standard [Mutual Non-Disclosure Agreement](https://drive.google.com/file/d/1hRAMBYrYcd9yG8FOItsfN0XYgdp32ajt/view) with the Vendor information. 
- Send the updated Mutual Non-Disclosure Agreement to the potential Vendor for signature. This should be done via HelloSign or other Electronic Signature method. 
- **DO NOT SIGN THE NDA YOURSELF** Only authorized individuals can executed contracts on behalf of GitLab. Please view the [Signatory Matrix](https://about.gitlab.com/handbook/finance/authorization-matrix/#authorization-matrix) for who may counter-sign the Mutual Non-Disclosure Agreement. 
- NOTE: If a potential vendor requires the use of their NDA template, please follow the process located on the [Procurement Page](https://about.gitlab.com/handbook/finance/procurement/) which will initiate the legal review process. 

## Negotiating Terms
- For any purchases made by GitLab, there must be applicable terms and conditions. 
- GitLab prefers to use the [Standard Terms and Conditions](https://about.gitlab.com/handbook/finance/procurement/vendor-guidelines/#standard-vendor-terms-and-conditions), which includes terms related to purchasing both software and/or professional services.
- For information regarding purchase requests and negotiation thresholds, please visit the [Procurement Toolkit](https://about.gitlab.com/handbook/finance/procurement/purchase-request-process/).

## Helpful Resources
- Many Vendors require basic information about GitLab to be setup as a Customer, visit [Company Information](https://gitlab.com/gitlab-com/finance/wikis/company-information) for general information about each GitLab legal entity
- GitLab's W9 can be found on the [Finance Page](https://about.gitlab.com/handbook/finance/#forms)
 
